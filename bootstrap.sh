#!/bin/bash

# dependencies
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install build-essential git python libssl-dev -y
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
echo "deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen" | sudo tee -a /etc/apt/sources.list.d/10gen.list
sudo apt-get update

# git, mongo and curl
sudo apt-get install -y git mongodb-10gen curl

# nodejs
cd /usr/local/src
sudo git clone git://github.com/joyent/node.git
cd node
sudo git checkout v0.10.18
sudo ./configure
sudo make
sudo make install

# meteor
curl https://install.meteor.com | sudo sh

# meteorite
sudo -H npm install -g meteorite --registry http://registry.npmjs.org/

# accommodate meteor local mongo db
echo "sudo mount --bind /home/vagrant/housematez/.meteor/ /vagrant/housematez/.meteor/" >> /home/vagrant/.bashrc && source /home/vagrant/.bashrc
echo "mrt create /home/vagrant/housematez" >> /home/vagrant/.bashrc && source /home/vagrant/.bashrc
echo "mrt create /vagrant/housematez" >> /home/vagrant/.bashrc && source /home/vagrant/.bashrc
echo "rm -Rf /vagrant/housematez/.meteor" >> /home/vagrant/.bashrc && source /home/vagrant/.bashrc
echo "mkdir /vagrant/housematez/.meteor" >> /home/vagrant/.bashrc && source /home/vagrant/.bashrc