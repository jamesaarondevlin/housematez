function TasksheetGenerationAlg()
{
	
}

TasksheetGenerationAlg.Generate = function()
{
	//all algorithms will need to remove
	//existing tasksheet data
	Meteor.call('removeAllTasksheetLines');
}

RotatingTasksheetGenerationAlg = function()
{
	TasksheetGenerationAlg.call(this);
}

RotatingTasksheetGenerationAlg.prototype = new TasksheetGenerationAlg();
RotatingTasksheetGenerationAlg.prototype.constructor = RotatingTasksheetGenerationAlg;

RotatingTasksheetGenerationAlg.prototype.Generate = function()
{
	TasksheetGenerationAlg.Generate();

	//discern average number of tasks each housemate ought to do
	var choreCount = Chores.find().count();
	var housemateCount = Housemates.find().count();
	var avgChoresPerHousematePerWeek = choreCount / housemateCount;
	var weeksForTasks = 4;
	var date = new Date();

	debugger;
	//allocate the tasks out for the next four weeks
	while(weeksForTasks != 0)
	{
		var choresToAssign = choreCount;
		var housemateIndex = 0;
		var housemate;
		var chore;

		var newTasksheetLine = {
			date : date,
			items : []
		};

		while(choresToAssign != 0)
		{
			chore = Chores.find().fetch()[choresToAssign - 1];

			if(housemateIndex < housemateCount)
			{
				housemate = Housemates.find().fetch()[housemateIndex];					
			}

			newTasksheetLine.items.push({
				chore : chore,
				housemate : housemate
			});

			choresToAssign--;
			housemateIndex++;
		}

		TasksheetLines.insert(newTasksheetLine);

		date.setDate(date.getDate() + 7);
		weeksForTasks--;
	}
}

RandomTasksheetGenerationAlg = function()
{
	TasksheetGenerationAlg.call(this);
}

RandomTasksheetGenerationAlg.prototype = new TasksheetGenerationAlg();
RandomTasksheetGenerationAlg.prototype.constructor = RandomTasksheetGenerationAlg;

RandomTasksheetGenerationAlg.prototype.Generate = function()
{
	TasksheetGenerationAlg.Generate();
}

TasksheetGenerationAlgFactory = function()
{
	this.GetGenerationAlgorithm = function(assignmentStrategy)
	{
		if(assignmentStrategy == "rotating")
		{
			return new RotatingTasksheetGenerationAlg();
		}

		if(assignmentStrategy == "random")
		{
			return new RandomTasksheetGenerationAlg();
		}
	}
}