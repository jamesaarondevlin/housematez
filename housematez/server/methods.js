if (Meteor.isServer) 
{
	Meteor.startup(function()
	{
		return Meteor.methods(
		{
			removeAllTasksheetLines: function() 
			{
				TasksheetLineItems.remove({});
        		TasksheetLines.remove({});
      		}
    	});
	});
}