Meteor.publish('housemates', function(){
	return Housemates.find();
});

Meteor.publish('chores', function(){
	return Chores.find();
});

Meteor.publish('tasksheet_lines', function(){
	return TasksheetLines.find();
});

Meteor.publish('tasksheet_line_items', function(){
	return TasksheetLineItems.find();
});