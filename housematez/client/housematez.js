Meteor.subscribe('housemates');
Meteor.subscribe('chores');
Meteor.subscribe('tasksheet_lines');
Meteor.subscribe('tasksheet_line_items')

Handlebars.registerHelper('choreItems', function()
{
	return Chores.find();
});

Handlebars.registerHelper('housemateItems', function()
{
	return Housemates.find();
});

generateTasksheet = function()
{
	var assignmentStrategy = $("#assignment_strategy").val();
	var tasksheetGenerationAlgFactory = new TasksheetGenerationAlgFactory();

	var tasksheetGenerationAlgorithm = 
		tasksheetGenerationAlgFactory.GetGenerationAlgorithm(assignmentStrategy);

	tasksheetGenerationAlgorithm.Generate();
}