Template.tasksheetLine.helpers({
	dateFormatted : function()
	{
		return this.date.toDateString();
	},

	tasksheetLineItems : function()
	{
		return TasksheetLineItems.find({tasksheetLine : this});
	}
})