Template.housemateItem.events({
	'click img[id=deleteHousemate]' : function(e)
	{
		e.preventDefault();
		Housemates.remove(this._id);
		generateTasksheet();
	}
})