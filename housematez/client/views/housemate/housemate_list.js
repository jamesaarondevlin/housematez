Template.housemateList.events({
	'click button[id=addNewHousemate]': function(e) 
	{
		e.preventDefault();
		insertNewHousemate();		
	},

	'keyup input[id=newHousemate]' : function(e)
	{
		if(e.which == 13)
		{
			e.preventDefault();
			insertNewHousemate();
		}
	}
});

insertNewHousemate = function()
{
	var housemate = {
		name: $('#newHousemate').val()
	}

	Housemates.insert(housemate);

	$("#newHousemate").val("");

	generateTasksheet();
}