Template.choreList.events({
	'click button[id=addNewChore]': function(e) 
	{
		e.preventDefault();
		insertNewChore();		
	},

	'keyup input[id=newChore]' : function(e)
	{
		if(e.which == 13)
		{
			e.preventDefault();
			insertNewChore();
		}
	}
});

insertNewChore = function()
{
	var chore = {
			title : $('#newChore').val()
	}

	Chores.insert(chore);

	$("#newChore").val("");

	generateTasksheet();
}